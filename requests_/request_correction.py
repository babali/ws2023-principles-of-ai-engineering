import requests

# Endpoint URL
url = 'http://127.0.0.1:5000/api/correct'

# Dummy JSON data
data = {
    "id": 4,
    "label": "question"
}

# Send POST request with JSON data and set Content-Type header
headers = {'Content-Type': 'application/json'}
response = requests.post(url, json=data, headers=headers)

# Print the response
print(response.json())