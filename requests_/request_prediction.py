import requests

# Endpoint URL
url = 'http://127.0.0.1:5000/api/predict'

# Dummy JSON data
data = {
    "title": "Is there an API I can use so I can upload in the terminal?",
    "body": "Firstly, this is a fantastic service and I love it! I'm trying to use it to send files securely, and I would love to do it programmatically. Is there an API I can use? I can't seem to find one. I know that timvisee created '[https://github.com/timvisee/ffsend](https://github.com/timvisee/ffsend)' which is amazing! It's just that it's not an official project and therefore I can't use it. Also, I'm not a real dev, I just dabble so everyone's patience is appreciated!"
}

# Send POST request with JSON data and set Content-Type header
headers = {'Content-Type': 'application/json'}
response = requests.post(url, json=data, headers=headers)

# Print the response
print(response.json())