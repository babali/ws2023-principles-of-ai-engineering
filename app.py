from flask import Flask, jsonify, request
from flask_cors import CORS
from pickle import load
from text_preprocess import preprocess_text
import sqlite3
from model.release import BlueGreenDeployment

app = Flask(__name__)
CORS(app)

# Create a connection to the SQLite database
conn = sqlite3.connect('issues.db')

# Choose release technique and get the main model
release_technique = BlueGreenDeployment(current_model='./data/current_model.pkl', new_model='./data/new_model.pkl', mode='debug')
main_model = release_technique.model()

# Load the trained model
with open(main_model, 'rb') as file:
    model_pipeline = load(file)

# Endpoint 1: Make prediction
@app.route('/api/predict', methods=['POST'])
def make_prediction():
    data = request.get_json()

    title = data.get('title', '')
    body = data.get('body', '')

    # Preprocess input data
    processed_data = ' '.join([title, body])  # Combine title and body
    processed_data = preprocess_text(processed_data)  # Implement the text preprocessing function

    # model_pipeline.named_steps['rf'].predict_proba(processed_data)
    # Make prediction using the model
    label = model_pipeline.predict([processed_data])[0]

    # Insert data into the SQLite database
    conn = sqlite3.connect('issues.db')
    cursor = conn.cursor()
    cursor.execute('INSERT INTO issues (title, body, label) VALUES (?, ?, ?)', (title, body, label))
    conn.commit()
    id = cursor.lastrowid
    cursor.close()
    conn.close()

    return jsonify({"id": id, "label": label})

# Endpoint 2: Make correction
@app.route('/api/correct', methods=['POST'])
def make_correction():
    data = request.get_json()

    id = data.get('id', '')
    label = data.get('label', '')
    
    # Insert data into the SQLite database
    conn = sqlite3.connect('issues.db')
    cursor = conn.cursor()
    cursor.execute('UPDATE issues SET label = ? WHERE id = ?', (label, id))
    conn.commit()
    cursor.close()
    conn.close()

    return jsonify({"id": id, "label": label})

if __name__ == '__main__':
    app.run(debug=True)
