import sqlite3

conn = sqlite3.connect('issues.db')

# Create a cursor object to interact with the database
cursor = conn.cursor()

# Execute SQL commands to create a table
cursor.execute('''
    CREATE TABLE issues (
        id INTEGER PRIMARY KEY,
        title TEXT NOT NULL,
        body TEXT NOT NULL,
        label TEXT
    )
''')

# Commit changes to the database
conn.commit()

# Close the cursor and connection when done
cursor.close()
conn.close()
