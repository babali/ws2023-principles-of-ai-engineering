import pytest
from app import app  # Import your Flask app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_predict_issue_type(client):
    # Test the /api/predict endpoint
    data = {
        "title": "Bug in Login",
        "body": "Encountered an issue while logging in."
    }
    response = client.post('/api/predict', json=data)
    assert response.status_code == 200
    assert 'id' in response.json
    assert 'label' in response.json

def test_correct_issue_type(client):
    # Test the /api/correct endpoint
    data = {
        "id": "2",
        "label": "bug"
    }
    response = client.post('/api/correct', json=data)
    assert response.status_code == 200
    assert 'id' in response.json
