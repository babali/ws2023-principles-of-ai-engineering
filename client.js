var correctedId = -1

function predictIssue() {
  // clean and freeze the radio options
  document.getElementsByName('predicted-label').forEach(option => {
    option.checked = false;
    option.disabled = true;
  });

  const title = document.getElementById('title').value;
  const body = document.getElementById('body').value;
  
  // function onLanguageDetected(langInfo) {
  //   for (const lang of langInfo.languages) {
  //     console.log(`Language is: ${lang.language}`);
  //     console.log(`Percentage is: ${lang.percentage}`);
  //   }
  // }

  // let text = "L'homme est né libre, et partout il est dans les fers.";
    
  // let detecting = browser.i18n.detectLanguage(text);
  // detecting.then(onLanguageDetected);
  // print(detecting);  

  // Make an API call to predict issue type
  fetch('http://localhost:5000/api/predict', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: title,
      body: body
    })
  })
  .then(response => response.json())
  .then(data => {
    // Handle the response data (data contains predicted label and issue ID)
    // console.log('Predicted label:', data.label);
    // console.log('Issue ID:', data.id);
      
    // heat/unfreeze options
    document.getElementsByName('predicted-label').forEach(option => {
      option.disabled = false;
    });

    document.getElementById(data.label).checked = true;

    correctedId = data.id;
  })
  .catch(error => {
    console.error('Error:', error);
  });
}
  
function submitPrediction() {
  const id = correctedId; // Get the issue ID from the user or the selected issue
  const correctedLabel = document.getElementsByName('predicted-label').value; // Get the corrected label from the user

  // Make an API call to submit corrected issue type
  fetch('http://localhost:5000/api/correct', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id: Number(id),
      label: correctedLabel
    })
  })
  .then(response => response.json())
  .then(data => {
    // Handle the response data (data contains issue ID)
    console.log('Corrected issue ID:', data.id);
    console.log('Corrected? issue label:', data.label);
  })
  .catch(error => {
    console.error('Error:', error);
  });
}