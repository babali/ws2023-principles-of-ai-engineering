from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, WordNetLemmatizer
import re
from pandas import notnull

# Text pre-processing functions
def tokenize(text):
    return word_tokenize(text)

def normalize(text):
    return text.lower()

def remove_noise(text):
    # Remove special characters, numbers, and punctuation
    return re.sub(r'[^a-zA-Z\s]', '', text)

def stem(text):
    stemmer = PorterStemmer()
    return ' '.join([stemmer.stem(word) for word in text.split()])

def lemmatize(text):
    lemmatizer = WordNetLemmatizer()
    return ' '.join([lemmatizer.lemmatize(word) for word in text.split()])

def remove_stopwords(text):
    stop_words = set(stopwords.words('english'))
    return ' '.join([word for word in text.split() if word not in stop_words])

def preprocess_text(x):
    return remove_stopwords(lemmatize(stem(remove_noise(normalize(x))))) if notnull(x) else ''