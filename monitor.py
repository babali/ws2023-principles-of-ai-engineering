from prometheus_client import start_http_server, Counter, Histogram
import pandas as pd
import nltk
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
import pickle
from text_preprocess import preprocess_text
from sklearn.metrics import accuracy_score
from collections import defaultdict

def monitor_single(model):
    nltk.download('wordnet')
    nltk.download('stopwords')

    # Load the trained model
    model_filename = model
    with open(model_filename, 'rb') as file:
        trained_model = pickle.load(file)

    # Load the dataset
    df = pd.read_csv('./data/sample1.csv')

    # Apply text pre-processing to the 'issue_title' column
    df['processed_title'] = df['issue_title'].apply(preprocess_text)

    # Apply text pre-processing to the 'issue_body' column
    df['processed_body'] = df['issue_body'].apply(preprocess_text)

    # Split the dataset into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(df[['processed_title', 'processed_body']], df['issue_label'], test_size=0.2, random_state=42)

    # Load the test data for evaluation
    X_test_data = X_test['processed_title'] + ' ' + X_test['processed_body']

    # Initialize Prometheus metrics
    accuracy = Counter('model_accuracy', 'Model Accuracy')
    average_confidence = Histogram('average_prediction_confidence', 'Average Prediction Confidence')
    predictions_per_category = Counter('predictions_per_category', 'Number of Predictions per Category', labelnames=['category'])
    correct_predictions_per_category = Counter('correct_predictions_per_category', 'Number of Correct Predictions per Category', labelnames=['category'])
    incorrect_predictions_per_category = Counter('incorrect_predictions_per_category', 'Number of Incorrect Predictions per Category', labelnames=['category'])

    # Make predictions on the test set
    predictions = trained_model.predict(X_test_data)
    prediction_probabilities = trained_model.predict_proba(X_test_data)

    # Calculate accuracy
    accuracy_value = accuracy_score(y_test, predictions)
    accuracy.inc(accuracy_value)

    return accuracy_value

    # Calculate metrics per category
    category_counts = defaultdict(int)
    correct_counts = defaultdict(int)
    incorrect_counts = defaultdict(int)

    for true_label, predicted_label, probs in zip(y_test, predictions, prediction_probabilities):
        category_counts[true_label] += 1
        predictions_per_category.labels(category=true_label).inc()

        if true_label == predicted_label:
            correct_counts[true_label] += 1
            correct_predictions_per_category.labels(category=true_label).inc()
        else:
            incorrect_counts[true_label] += 1
            incorrect_predictions_per_category.labels(category=true_label).inc()

        average_confidence.observe(probs.max())

    # Print the metrics for verification
    print(f"Accuracy: {accuracy_value}")
    print("Predictions per category:", dict(category_counts))
    print("Correct predictions per category:", dict(correct_counts))
    print("Incorrect predictions per category:", dict(incorrect_counts))

    # Start the Prometheus server
    start_http_server(8000)
    while True:
        pass  # Keep the server running

def monitor(current_model, new_model):
    return monitor_single(new_model) > monitor_single(current_model)

    if monitor_single(new_model) > monitor_single(current_model):
        main_model = './data/main_model.pkl'
        with open(main_model, 'wb') as wb_file,  open(new_model, 'rb') as rb_file:
            pickle.dump(pickle.load(new_model, rb_file), wb_file)


if __name__ == "__main__":
    nltk.download('wordnet')
    nltk.download('stopwords')

    # Load the trained model
    model_filename = './data/random_forest_model_full.pkl'
    with open(model_filename, 'rb') as file:
        trained_model = pickle.load(file)

    # Load the dataset
    df = pd.read_csv('./data/sample1.csv')

    # Apply text pre-processing to the 'issue_title' column
    df['processed_title'] = df['issue_title'].apply(preprocess_text)

    # Apply text pre-processing to the 'issue_body' column
    df['processed_body'] = df['issue_body'].apply(preprocess_text)

    # Split the dataset into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(df[['processed_title', 'processed_body']], df['issue_label'], test_size=0.2, random_state=42)

    # Load the test data for evaluation
    X_test_data = X_test['processed_title'] + ' ' + X_test['processed_body']

    # Initialize Prometheus metrics
    accuracy = Counter('model_accuracy', 'Model Accuracy')
    average_confidence = Histogram('average_prediction_confidence', 'Average Prediction Confidence')
    predictions_per_category = Counter('predictions_per_category', 'Number of Predictions per Category', labelnames=['category'])
    correct_predictions_per_category = Counter('correct_predictions_per_category', 'Number of Correct Predictions per Category', labelnames=['category'])
    incorrect_predictions_per_category = Counter('incorrect_predictions_per_category', 'Number of Incorrect Predictions per Category', labelnames=['category'])

    # Make predictions on the test set
    predictions = trained_model.predict(X_test_data)
    prediction_probabilities = trained_model.predict_proba(X_test_data)

    # Calculate accuracy
    accuracy_value = accuracy_score(y_test, predictions)
    accuracy.inc(accuracy_value)

    # Calculate metrics per category
    category_counts = defaultdict(int)
    correct_counts = defaultdict(int)
    incorrect_counts = defaultdict(int)

    for true_label, predicted_label, probs in zip(y_test, predictions, prediction_probabilities):
        category_counts[true_label] += 1
        predictions_per_category.labels(category=true_label).inc()

        if true_label == predicted_label:
            correct_counts[true_label] += 1
            correct_predictions_per_category.labels(category=true_label).inc()
        else:
            incorrect_counts[true_label] += 1
            incorrect_predictions_per_category.labels(category=true_label).inc()

        average_confidence.observe(probs.max())

    # Print the metrics for verification
    print(f"Accuracy: {accuracy_value}")
    print("Predictions per category:", dict(category_counts))
    print("Correct predictions per category:", dict(correct_counts))
    print("Incorrect predictions per category:", dict(incorrect_counts))

    # Start the Prometheus server
    start_http_server(8000)
    while True:
        pass  # Keep the server running
