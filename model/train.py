import pandas as pd
import nltk
from text_preprocess import preprocess_text
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
import pickle


nltk.download('wordnet')
nltk.download('stopwords')

# Load the dataset
df = pd.read_csv('./data/sample1.csv')

# Apply text pre-processing to the 'issue_title' column
df['processed_title'] = df['issue_title'].apply(preprocess_text)

# Apply text pre-processing to the 'issue_body' column
df['processed_body'] = df['issue_body'].apply(preprocess_text)

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(df[['processed_title', 'processed_body']], df['issue_label'], test_size=0.2, random_state=42)

# Build a random forest model pipeline
model_pipeline = Pipeline([
    ('tfidf', TfidfVectorizer()),
    ('rf', RandomForestClassifier())
])

# Train the model
model_pipeline.fit(X_train['processed_title'] + ' ' + X_train['processed_body'], y_train)

# Save the trained model to a file
model_filename_pickle = './data/random_forest_model_full.pkl'
with open(model_filename_pickle, 'wb') as file:
    pickle.dump(model_pipeline, file)
