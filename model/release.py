from abc import ABC, abstractmethod
from datetime import datetime

class ReleaseTechnique(ABC):
    def __init__(self, current_model, new_model) -> None:
        self.current_model = current_model
        self.new_model = new_model

    @abstractmethod
    def model(self):
        raise NotImplementedError()

class RollingRelease(ReleaseTechnique):
    def __init__(self, current_model, new_model, release_date: datetime) -> None:
        super(current_model, new_model).__init__()

        self.release_date = release_date

    def model(self):
        if datetime.now() > self.release_date:
            return self.new_model
        else:
            return self.current_model

class BlueGreenDeployment(ReleaseTechnique):
    def __init__(self, current_model, new_model, mode: str) -> None:
        super(current_model, new_model).__init__()

        self.mode = mode

    def model(self):
        if self.mode == "debug":
            return self.new_model
        else:
            return self.current_model

class CanaryRelease(ReleaseTechnique):
    def __init__(self, current_model, new_model, user) -> None:
        super(current_model, new_model).__init__()

        self.user = user

    def model(self):
        if self.user.is_canary:
            return self.new_model

        return self.current_model

class FeatureToggles(ReleaseTechnique):
    def __init__(self, models: dict, user) -> None:
        self.models = models
        self.user = user

    def model(self):
        return self.models[self.user.feature]
