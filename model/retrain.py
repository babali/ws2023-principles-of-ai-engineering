import pandas as pd
from text_preprocess import preprocess_text
from pickle import load, dump
# from monitor import monitor


# Load the trained model
with open('./data/random_forest_model_full.pkl', 'rb') as file:
    model_pipeline = load(file)

# df = pd.read_csv('./data/sample1.csv')
df_new = pd.read_csv('./data/sample2.csv')

# Apply text pre-processing to relevant columns
df_new['processed_title'] = df_new['issue_title'].apply(preprocess_text)

# Apply text pre-processing to the 'issue_body' column
df_new['processed_body'] = df_new['issue_body'].apply(preprocess_text)

# Combine datasets
# df_combined = pd.concat([df, df_new], axis=0)
df_combined = df_new

X_combined = df_combined[['processed_title', 'processed_body']]
y_combined = df_combined['issue_label']

# Update the model pipeline accordingly
model_pipeline.fit(X_combined['processed_title'] + ' ' + X_combined['processed_body'], y_combined)

model_filename_updated = './data/random_forest_model_updated.pkl'
with open(model_filename_updated, 'wb') as file:
    dump(model_pipeline, file)

# if monitor("./data/random_forest_model.pkl", "random_forest_model_updated.pkl"):
#     main_model = './data/main_model.pkl'
#     with open(main_model, 'wb') as file:
#         dump(model_pipeline, file)